var $container = document.getElementById('contenedor')

// Add transition prop into next tick (some weird Chrome bug).
setTimeout(function () {
  $container.style.transition = 'all .3s'
})

function showModal () {
  $container.classList.add('is-visible')
}

function hideModal () {
  $container.classList.remove('is-visible')
}
