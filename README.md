# OneStep - Coming soon web template

Simple "coming-soon" website template for OneStep project.

## Development

First of all, you need to clone the repository standing in your local server directory e.g. `htdocs`:

```bash
$ git clone gitlab:motion-sas/onestep/soon-web
```

Then, start your server...
