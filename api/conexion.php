<?php
 $config = parse_ini_file(__DIR__ .'/../db_config.ini');
    $db = new PDO(
        $config['driver'].
        ":host={$config['host']}".
        ";dbname={$config['name']}",
        $config['user'],
        $config['password']
    );
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

